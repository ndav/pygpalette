================
PygPalette
================

A small python module to generate GIMP-style palette files (also used with, e.g., Inkscape). Color values were nicked from `webucator.com <https://www.webucator.com/blog/2015/03/python-color-constants-module/>`_ which contains no license info.


------------
Usage
------------

^^^^^^^
Package
^^^^^^^

Not yet stable. Classes and function names will most likely change in the near future.

^^^^^^^^^^^
Comand Line
^^^^^^^^^^^

The command line entry point is "pygpalette". Input is via command-line switches and/or an input file containing one color string per line. Color strings may be any of web-style hex strings (e.g., #A48C32), an RGB triplet (e.g., 54,12,231 - no spaces at present), or a color name like "orange". If both an input file and command-line values are given, file values are output first, followed by any colors provided on the command-line. Output is ASCII text to stdout.

Example::
  pygpalette -f input_file.txt -c 55,77,99 -c purple -c #ff0000 TestPalette


The module understands roughly 550 colors by name. More may be added in the future.
  
