"""GIMP Palette file generation code."""
from . import colors

def generate_header(name):
    """Generate header strings for a GIMP Palette file.

    :param name: The name of the palette.
    :type name: str
    :return: List of header text lines.
    :rtype: list
    """
    return ['GIMP Palette', f'Name: {name}', '',
                '# Automatically generated by pyg_palette',
                '# [INSERT URL HERE]', '']

def iterate_colors(color_list):
    """Generator that iterates individual GIMP Palette color value lines.

    :param color_list: Iterable of color values in web-style hex format, or
        a tuple-like of (Red, Green, Blue) values.
    :type color_list: str or tuple
    :return: iterable of str
    """
    for color in color_list:
        v = colors.RGB.from_value(color)
        yield f'{v.red:>3} {v.green:>3} {v.blue:>3}    {v.name} ({v.hex})'

def iterate_palette(name, color_list, comments=None):
    """Generate an iterable that returns the lines for a GIMP Palette file.

    :param name: The name for the palette.
    :param color_list: A list of colors to include in the palette. Accepts
        web-stype hex strings, name strings, or (R, G, B) triplets in any
        combination.
    :type name: str
    :type color_list: list of str or tuple
    :return: iterable of str
    """
    for line in generate_header(name):
        yield line

    if comments is not None:
        for line in comments:
            yield f'# {line}'
            
    for line in iterate_colors(color_list):
        yield line
        
                 
