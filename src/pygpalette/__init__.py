from .gpl import *

def script_entry():
    import argparse
    import pathlib

    import pygpalette

    p = argparse.ArgumentParser()

    p.add_argument('-f', '--file', type=pathlib.Path,
                   help="File containing colors, one per line.")
    p.add_argument('-c', '--color', action='append', type=str,
                   help='Color to add. May be repeated.')
    p.add_argument('-i', '--info', action='append', type=str,
                   help='Add a comment line. May be repeated.')
    p.add_argument('name', type=str, help='Palette name')
    args = p.parse_args()

    colors = []
    if args.file is not None:
        with args.file.open('r') as f:
            for line in f:
                line = line.strip()
                if line=='' or line.startswith('//'):
                    continue
                if line.count(',') == 2:
                    line = tuple([int(x) for x in line.split(',')])
                colors.append(line)
    if args.color is not None:
        for c in args.color:
            if c.count(',') == 2:
                c = tuple([int(x) for x in c.split(',')])
            colors.append(c)

    for line in pygpalette.iterate_palette(args.name, colors, args.info):
        print(line)

